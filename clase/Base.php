<?php

/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 1/26/2021
 * Time: 12:07 PM
 */
class Base
{
    private $id;
    /**
     * Comment constructor.
     * @param $id
     */
    public function __construct($id = null)
    {
        $this->setId($id);
        if (!is_null($id)) {
            $data = query("SELECT * FROM " . strtolower(get_class($this)) . " WHERE id=" . intval($this->id));

            foreach ($data[0] as $key => $value) {
                $this->$key = $value;
            }
        }
    }
    static public function findBy($filters = [], $orderBy = null, $orderDirection = 'ASC', $limit = 0, $offset = 0)
    {

        // generalizare query pentru selectia din tabelul clasei curente
        $query = "SELECT id FROM " . strtolower(static::class) . " ";

        // verificare daca userul a trimis filtru
        if (count($filters) > 0) {
            $fields = [];
            //ia fiecare filtru si il trasnforma ex ("tip_imobil='garsoniera'")
            foreach ($filters as $key => $value) {
                $fields[] = "$key='$value'";
            }
            // transforma lista de fintre intr-un string ex ("tip_imobil='garsoniera' AND "nr_camere='5'...")
            $where = implode(' AND ', $fields);

            $query .= " WHERE $where ";
        }
        //daca userul vrea si sortare
        if (!is_null($orderBy)) {
            $query .= " ORDER BY $orderBy $orderDirection";
        }
        //verifica daca userul a solicitat limitarea rezultatelor
        if ($limit > 0) {
            $query .= " LIMIT $offset,$limit";
        }

        //extragem informatiile din DB folosind query-ul contruit mai sus
        // data este un array cu toate informatiile din tabela care respecta condtiile de mai sus
        $data = query($query);

        $objects = [];
        //obtinem numele clasei
        $className = static::class;
        //generam lista de obiecte pe baza informatiilor obitnute din DB
        foreach ($data as $objId) {
            $objects[] = new $className($objId['id']);
        }

        return $objects;
    }

    static public function findOneBy($filters = [], $orderBy = null, $orderDirection = 'ASC')
    {
        $objects = static::findBy($filters, $orderBy, $orderDirection, 1);

        return $objects[0];

    }

    static public function find($id)
    {
        return static::findOneBy(['id' => $id]);
    }



    public function delete()
    {
        query("DELETE FROM " . strtolower(get_class($this)) . " WHERE id=" . intval($this->getId()));
    }



    public function create(){
        $values = get_object_vars($this);
        unset($values['id']);
        $keys = implode('`,`',array_keys($values));
        $vals = implode("', '", $values);

        $id = query("INSERT INTO ".strtolower(get_class($this))." (`".$keys."`) VALUES ('".$vals."')");
        $this->setId($id);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Base
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }


}