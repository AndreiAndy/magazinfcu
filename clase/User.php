<?php

/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 1/29/2021
 * Time: 1:20 PM
 */
class User extends Base
{
public $utilizator;

public $parola;

    /**
     * @return mixed
     */
    public function getUtilizator()
    {
        return $this->utilizator;
    }

    /**
     * @param mixed $utilizator
     * @return User
     */
    public function setUtilizator($utilizator)
    {
        $this->utilizator = $utilizator;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getParola()
    {
        return $this->parola;
    }

    /**
     * @param mixed $parola
     * @return User
     */
    public function setParola($parola)
    {
        $this->parola = $parola;
        return $this;
    }


}