<?php

/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 1/26/2021
 * Time: 12:40 PM
 */
class Category extends Base
{
 public $nume;


    public function GetProduct()
    {
        return Product::findBy(['category_id'=>intval($this->getId())]);
    }
    /**
     * @return mixed
     */
    public function getNume()
    {
        return $this->nume;
    }

    /**
     * @param mixed $nume
     * @return Category
     */
    public function setNume($nume)
    {
        $this->nume = $nume;
        return $this;
    }


}