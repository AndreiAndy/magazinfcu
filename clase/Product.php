<?php

/**
 * Created by PhpStorm.
 * User: Andy
 * Date: 1/26/2021
 * Time: 12:24 PM
 */
class Product extends Base
{
public $nume;

public $pret;

public $poza;

public $category_id;

    /**
     * @return mixed
     */
    public function getNume()
    {
        return $this->nume;
    }

    /**
     * @param mixed $nume
     * @return Product
     */
    public function setNume($nume)
    {
        $this->nume = $nume;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPret()
    {
        return $this->pret;
    }

    /**
     * @param mixed $pret
     * @return Product
     */
    public function setPret($pret)
    {
        $this->pret = $pret;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPoza()
    {
        return $this->poza;
    }

    /**
     * @param mixed $poza
     * @return Product
     */
    public function setPoza($poza)
    {
        $this->poza = $poza;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategoryId()
    {
        return $this->category_id;
    }

    /**
     * @param mixed $category_id
     * @return Product
     */
    public function setCategoryId($category_id)
    {
        $this->category_id = $category_id;
        return $this;
    }


}