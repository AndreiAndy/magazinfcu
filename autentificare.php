<?php include "functions.php" ;?>
<head>
    <meta charset="UTF-8">
    <title>Autentificare</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</head>
<body>
 <div class="container">
     <div id="header" class="row" style="background-color: blue" >
         <div class="col-3"> <img src="poze/siglafcu.png"></div>
         <div class="col-9"><h1>Magazin FC Universitatea Craiova</h1></div>
     </div>
     <div id="menu" class="row">
         <nav class="navbar navbar-expand-lg navbar-dark bg-dark col-12">
             <a class="navbar-brand" href="index.php">Acasa</a>
             <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                 <span class="navbar-toggler-icon"></span>
             </button>

             <div class="collapse navbar-collapse" id="navbarSupportedContent">
                 <ul class="navbar-nav mr-auto">
                     <li class="nav-item dropdown">
                         <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                             Produse
                         </a>
                         <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                             <?php
                             $categories = Category::findBy();
                             foreach ($categories as $category) {
                                 echo '<a  class="dropdown-item" href="category.php?id=' . $category->getId() . '">' . $category->getNume() . '</a>';
                             }
                             ?>
                         </div>
                     </li>
                     <li class="nav-item">
                         <a class="nav-link" href="inregistrare.php">Inregistrare</a>
                     </li>
                     <li class="nav-item">
                         <a class="nav-link" href="autentificare.php">Autentificare</a>
                     </li>
                     <li class="nav-item">
                         <a class="nav-link" href="cos.php"><i class="fas fa-shopping-basket"></i></a>
                         <a class="nav-link" href="cos.php">Cos</a>
                     </li>
                 </ul>
                 <form class="form-inline my-2 my-lg-0">
                     <input class="form-control mr-sm-2" type="search" placeholder="Cauta" aria-label="Search">
                     <button class="btn btn-outline-success my-2 my-sm-0"   type="submit">Cauta</button>
                 </form>
             </div>
         </nav>
     </div>
     <div class="row">
         <div class="col-6 card">
             <h2>Autentificare</h2>

             <form action="loginCheck.php" method="post">
                 <div class="form-group">
                     <label for="exampleInputEmail1">Utilizator</label>
                     <input type="text" name="username" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                 </div>
                 <div class="form-group">
                     <label for="exampleInputPassword1">Parola</label>
                     <input type="password" name="password" class="form-control" id="exampleInputPassword1">
                 </div>
                 <button type="submit" class="btn btn-primary">Autentificare</button>
             </form>
         </div>
         <div class="col-6 card">
             <h5 class="card-title">SUNT CLIENT NOU</h5>
             <p class="card-text">Prin crearea unui cont vei putea face cumparaturile mai rapid, vei fi la curent cu statusurile comenzilor tale, si vei tine evidenta comenzilor efectuate de tine.</p>
             <a href="inregistrare.php" class="btn btn-primary">Creaza un cont</a>
         </div>
     </div>
     <div id="footer" class="row" style="background-color: lightblue">
         <div class="col-4">&copy; 2020</div>
         <div class="col-4">
             <ul>Contact</ul>
             <li>Str. Sf Dumitru, nr. 1, Craiova</li>
             <li>07xxxxxxx</li>
             <li>fcucraiovaofficial@gmail.com</li>
         </div>
         <div class="col-4">
             <ul>Program</ul>
             <li>luni-vineri:09:00-18:00</li>
             <li>sambata:10:00-15:00</li>
             <li>duminica:inchis</li>
         </div>
     </div>
 </div>
</body>